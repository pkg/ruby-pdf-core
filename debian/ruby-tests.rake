require 'rspec/core/rake_task'

RSpec::Core::RakeTask.new("spec") do |c|
  c.rspec_opts = "-t ~unresolved"
end

task :default => [:spec]
